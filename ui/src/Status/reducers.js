import {
  GET_STATUS_PENDING, GET_STATUS_FULFILLED, GET_STATUS_REJECTED
} from "./actions";

const initialState = {
  status: {baseUrl: "http://home"},
  statusLoading: false,
  statusError: null
};

export const statusReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_STATUS_PENDING:
      return {
        ...state,
        statusLoading: true
      };
    case GET_STATUS_FULFILLED:
      return {
        ...state,
        status: action.payload.response.data,
        statusLoading: false,
        statusError: null
      };
    case GET_STATUS_REJECTED:
      return {
        ...state,
        status: {baseUrl: "http://home"},
        statusLoading: false,
        statusError: action.payload.error
      };
    default:
      return state;
  }
};
