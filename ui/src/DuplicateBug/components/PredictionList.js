import * as React from "react";

import { ListView } from "patternfly-react";
import PredictionListItem from "./PredictionListItem";

class PredictionList extends React.Component {


  render() {
    const {prediction, records} = this.props;

    let items = records;
    if (prediction && prediction.records && prediction.records.length > 0) {
      items = [];
      records.forEach(r => {
        for (let i = 0; i < prediction.records.length; i++) {
          if (prediction.records[i].id === r.id) {
            items.push(prediction.records[i]);
            break;
          }

          //TODO remove
          //TEMP DEBUG
          // if (prediction.records[i].title === r.title) {
          //   items.push(prediction.records[i]);
          //   break;
          // }
          //TEMP DEBUG
        }
      })
    }

    return (
      <ListView>
        {items.map((x, index) => {
          return (
            <PredictionListItem
              key={index}
              num={index}
              initExpanded={index === 0}
              bug={x}/>)
        })}
      </ListView>
    );
  }
}

export default PredictionList;
