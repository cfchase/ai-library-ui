const uuid = require("uuid");
const Joi = require("joi");
const servicePrefix = require("../../constants").servicePrefix;
const predictionDataPrefix = require("../../constants").predictionDataPrefix;
const predictionDataRecordsPrefix = require("../../constants").predictionDataRecordsPrefix;
const storageErrorResponseBody = require("../../../utilities").storageErrorResponseBody;

handler = async (request, h) => {
  const storage = request.server.app.aiLibrary.storage;
  const id = uuid.v4();
  const fileKey = `${servicePrefix}/${predictionDataPrefix}/${request.params.setId}/${predictionDataRecordsPrefix}/${id}.json`;
  const dateStr = new Date().toISOString();
  const obj = {
    ...request.payload,
    id,
    createdAt: dateStr,
    modifiedAt: dateStr
  };

  try {
    await storage.writeJson(fileKey, obj);
    return h.response({
      metadata: {
        type: "FlakePredictionDataRecord",
        async: {
          status: "success"
        }
      },
      data: obj
    }).code(201);
  }
  catch (err) {
    console.error(err);
    let responseBody = storageErrorResponseBody(err);
    return h.response(responseBody).code(responseBody.statusCode);
  }
};

validate = {
  payload: {
    label: Joi.string(),
    log: Joi.string().required()
  }
};

module.exports.handler = handler;
module.exports.validate = validate;

