addPredictionRecordRoutes = (server) => {
  server.route({
    method: ["GET"],
    path: "/api/duplicate/predictions/{predictionId}/records/{id}",
    handler: require("./get").handler
  });

  server.route({
    method: ["GET"],
    path: "/api/duplicate/predictions/{predictionId}/records",
    handler: require("./list").handler
  });
};

module.exports = addPredictionRecordRoutes;