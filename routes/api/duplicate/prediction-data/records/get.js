const servicePrefix = require("../../constants").servicePrefix;
const predictionDataPrefix = require("../../constants").predictionDataPrefix;
const predictionDataRecordsPrefix = require("../../constants").predictionDataRecordsPrefix;
const storageErrorResponseBody = require("../../../utilities").storageErrorResponseBody;

handler = async (request, h) => {
  const storage = request.server.app.aiLibrary.storage;
  const id = request.params.id;
  const fileKey = `${servicePrefix}/${predictionDataPrefix}/${request.params.setId}/${predictionDataRecordsPrefix}/${id}.json`;

  try {
    let obj = await storage.readJson(fileKey);
    return h.response({
      metadata: {
        type: "DuplicatePredictionDataRecord",
        async: {
          status: "success"
        }
      },
      data: obj
    }).code(200);
  }
  catch (err) {
    console.error(err);
    let responseBody = storageErrorResponseBody(err);
    return h.response(responseBody).code(responseBody.statusCode);
  }
};

module.exports.handler = handler;