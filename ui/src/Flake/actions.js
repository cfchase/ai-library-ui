import axios from "axios"
import urljoin from "url-join";

import { createAxiosErrorNotification } from "../Notifications/actions";

export const RESET_FLAKE_PREDICTION = "RESET_FLAKE_PREDICTION";
export const resetFlakePrediction = () => ({
  type: RESET_FLAKE_PREDICTION,
  payload: {}
});

export const CREATE_FLAKE_PREDICTION_DATA_FULFILLED = "CREATE_FLAKE_PREDICTION_DATA_FULFILLED";
export const createFlakePredictionDataFulfilled = (response) => ({
  type: CREATE_FLAKE_PREDICTION_DATA_FULFILLED,
  payload: {
    response
  }
});

export const CREATE_FLAKE_PREDICTION_RECORD_FULFILLED = "CREATE_FLAKE_PREDICTION_RECORD_FULFILLED";
export const createFlakePredictionRecordFulfilled = (response) => ({
  type: CREATE_FLAKE_PREDICTION_RECORD_FULFILLED,
  payload: {
    response
  }
});

export const GET_FLAKE_PREDICTION_PENDING = "GET_FLAKE_PREDICTION_PENDING";
export const getFlakePredictionPending = (requestDate) => ({
  type: GET_FLAKE_PREDICTION_PENDING,
  payload: {
    requestDate
  }
});

export const GET_FLAKE_PREDICTION_FULFILLED = "GET_FLAKE_PREDICTION_FULFILLED";
export const getFlakePredictionFulfilled = (response) => ({
  type: GET_FLAKE_PREDICTION_FULFILLED,
  payload: {
    response
  }
});

export const GET_FLAKE_PREDICTION_REJECTED = "GET_FLAKE_PREDICTION_REJECTED";
export const getFlakePredictionRejected = (error) => ({
  type: GET_FLAKE_PREDICTION_REJECTED,
  payload: {
    error
  }
});

export const getFlakePrediction = (id) => {
  return async function (dispatch) {
    dispatch(getFlakePredictionPending(new Date()));
    try {
      let response = await axios.get(`/api/flake/predictions/${id}?include=records`);
      dispatch(getFlakePredictionFulfilled(response));
    }
    catch (error) {
      dispatch(createAxiosErrorNotification(error));
      dispatch(getFlakePredictionRejected(error));
    }
  }
};

export const CREATE_FLAKE_PREDICTION_PENDING = "CREATE_FLAKE_PREDICTION_PENDING";
export const createFlakePredictionPending = () => ({
  type: CREATE_FLAKE_PREDICTION_PENDING,
  payload: {}
});

export const CREATE_FLAKE_PREDICTION_FULFILLED = "CREATE_FLAKE_PREDICTION_FULFILLED";
export const createFlakePredictionFulfilled = (response) => ({
  type: CREATE_FLAKE_PREDICTION_FULFILLED,
  payload: {
    response
  }
});

export const CREATE_FLAKE_PREDICTION_REJECTED = "CREATE_FLAKE_PREDICTION_REJECTED";
export const createFlakePredictionRejected = (error) => ({
  type: CREATE_FLAKE_PREDICTION_REJECTED,
  payload: {
    error
  }
});

export const createFlakePrediction = (logs, modelId) => {
  return async function (dispatch) {
    dispatch(createFlakePredictionPending());
    try {
      let td = await _createFlakePredictionData(dispatch, logs);
      let response = await axios.post("/api/flake/predictions", {modelId, predictionDataId: td.id});
      dispatch(createFlakePredictionFulfilled(response));
    }
    catch (error) {
      dispatch(createAxiosErrorNotification(error));
      dispatch(createFlakePredictionRejected(error));
    }
  }
};

async function _createFlakePredictionData(dispatch, logs) {
  const predictionDataUrl = "/api/flake/prediction-data";
  let response = await axios.post(predictionDataUrl, {});
  dispatch(createFlakePredictionDataFulfilled(response));

  let predictionData = response.data.data;
  let recordPromises = [];

  logs.forEach(l => {
    recordPromises.push(
      axios.post(urljoin(predictionDataUrl, predictionData.id, "records"), l)
    )
  });

  let records = await Promise.all(recordPromises);
  dispatch(createFlakePredictionRecordFulfilled(records[0]));

  return predictionData;
}


