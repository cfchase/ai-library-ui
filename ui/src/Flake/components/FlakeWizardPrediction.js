import * as React from "react";
import { connect } from "react-redux";

import { Grid, Tab, Tabs } from "patternfly-react";
import moment from "moment";
import lodashGet from "lodash/get";

import AxiosError from "../../AxiosError/components/AxiosError";
import CodeSample from "../../CodeSample/components/CodeSample";
import JsonSample from "../../JsonSample/components/JsonSample";
import PredictionResultsTableGrid from "./PredictionResultsTableGrid";
import { generateCurl } from "./sampleData";

function generatePredictionCurl(status, prediction) {
  const createPredictionCurl = generateCurl(
    'POST',
    status.baseUrl,
    "api/flake/predictions",
    {modelId: prediction.modelId, predictionDataId: prediction.predictionDataId});

  const getPredictionCurl = generateCurl(
    'GET',
    status.baseUrl,
    `api/flake/predictions/${prediction.id}?include=records`);

  return {createPredictionCurl, getPredictionCurl};
}

class FlakeWizardPrediction extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tabActiveKey: 0
    }
  }

  componentWillUnmount() {
    clearInterval(this.state.pollingTimer);
  }

  selectTab = (tabActiveKey) => {
    this.setState({tabActiveKey});
  };

  renderError = () => {
    return <AxiosError error={this.props.predictionError}/>
  };


  renderPending = () => {
    const {status, predictionLoading, prediction, getPredictionDate, getPredictionResponse, createPredictionResponse} = this.props;

    if (!prediction) {
      return (
        <div className="flake-wizard-contents flake-results flake-pending">
          <div>
            <span className="spinner spinner-xl spinner-inline"/>
          </div>
        </div>
      )
    }

    const {createPredictionCurl, getPredictionCurl} = generatePredictionCurl(status, prediction);
    let pollingText = <p><span className="spinner spinner-xs spinner-inline"/> No requests made yet.  Waiting...</p>;
    if (predictionLoading) {
      pollingText = <p><span className="spinner spinner-xs spinner-inline"/> {`Last request made at ${moment(getPredictionDate).format("LTS")}. Requesting...`}</p>;
    } else if (getPredictionDate) {
      pollingText = <p><span className="spinner spinner-xs spinner-inline"/> {`Last request made at ${moment(getPredictionDate).format("LTS")}. Waiting...`}</p>;
    }

    let response;
    if (getPredictionResponse) {
      response = <JsonSample title="Last Response" object={getPredictionResponse}/>
    } else {
      response = (
        <div className="JsonSample json-sample-section">
          <div className="json-sample-heading">
            <h4 className="json-sample-title">Last Response </h4>
            <div className="prediction-response-pending"><span className="spinner spinner-md spinner-inline"/></div>
          </div>
        </div>);
    }

    let asyncStatus = lodashGet(getPredictionResponse, "metadata.async.status") || lodashGet(createPredictionResponse, "metadata.async.status");

    return (
      <div className="flake-wizard-contents flake-results flake-success">
        <div className="flake-info-section">
          <h2>Prediction Job in Progress</h2>
          <p>We've created the job to run the prediction and are currently polling for the results. The job status
            is <code>{asyncStatus}</code>. Results will be shown as
            the job completes
          </p>
        </div>
        <Tabs
          activeKey={1}
          onSelect={() => null}
          animation={false}
          id="flakeResultsTabs"
        >
          <Tab eventKey={0} title={<span><span className="spinner spinner-xs spinner-inline"/> Results</span>} disabled>
          </Tab>
          <Tab eventKey={1} title="API">
            <div className="flake-info-section">
              <h2>Create a Prediction</h2>
              <h3>Start the Prediction Job (Async)</h3>
              <Grid.Row>
                <Grid.Col className="sample-request-col" lg={6}>
                  <CodeSample title="Request" code={createPredictionCurl} language="bash"/>
                </Grid.Col>
                <Grid.Col className="sample-response-col" lg={6}>
                  <JsonSample title="Response" object={createPredictionResponse}/>
                </Grid.Col>
              </Grid.Row>
              <h3>Poll the Async Creation of the Prediction</h3>
              {pollingText}
              <Grid.Row>
                <Grid.Col className="sample-request-col" lg={6}>
                  <CodeSample title="Last Request" code={getPredictionCurl} language="bash"/>
                </Grid.Col>
                <Grid.Col className="sample-response-col" lg={6}>
                  {response}
                </Grid.Col>
              </Grid.Row>
            </div>
          </Tab>
        </Tabs>
      </div>
    );
  };

  renderResults = () => {
    const {tabActiveKey} = this.state;
    const {status, prediction, getPredictionResponse, createPredictionResponse} = this.props;
    const {createPredictionCurl, getPredictionCurl} = generatePredictionCurl(status, prediction);

    return (
      <div className="flake-wizard-contents flake-results flake-success">
        <Tabs
          activeKey={tabActiveKey}
          onSelect={this.selectTab}
          animation={false}
          id="flakeResultsTabs"
        >
          <Tab eventKey={0} title="Results">
            <h2>Prediction Results</h2>
            <p>After executing an analysis of the data, a flake probability will be added to a prediction data record
              and returned as the prediction results in the field <code>flake</code>. This probability represents the
              likelihood that this result can be ignored as a flake.
            </p>
            <JsonSample title={`Prediction Result Record (1 of ${prediction.records.length})`} object={prediction.records[0]}/>
            <h3>All Results</h3>
            <PredictionResultsTableGrid items={prediction.records}/>
          </Tab>
          <Tab eventKey={1} title="API">
            <div className="flake-info-section">
              <h2>Create a Prediction</h2>
              <h3>Start the Prediction Job (Async)</h3>
              <Grid.Row>
                <Grid.Col className="sample-request-col" lg={6}>
                  <CodeSample title="Request" code={createPredictionCurl} language="bash"/>
                </Grid.Col>
                <Grid.Col className="sample-response-col" lg={6}>
                  <JsonSample title="Response" object={createPredictionResponse}/>
                </Grid.Col>
              </Grid.Row>
              <h3>Poll the Async Creation of the Prediction</h3>
              <Grid.Row>
                <Grid.Col className="sample-request-col" lg={6}>
                  <CodeSample title="Request" code={getPredictionCurl} language="bash"/>
                </Grid.Col>
                <Grid.Col className="sample-response-col" lg={6}>
                  <JsonSample title="Response" object={getPredictionResponse}/>
                </Grid.Col>
              </Grid.Row>
            </div>
          </Tab>
        </Tabs>
      </div>
    );
  };

  renderNoResults = () => {
    return (
      <div className="flake-wizard-contents flake-results flake-no-results">
        <h3>No Results</h3>
      </div>
    );
  };

  render() {
    const {prediction, predictionLoading, predictionAsync, predictionError} = this.props;
    // TODO: REVISIT and change from status !== success to status === "in_progress"
    // Keep polling in case of failure as a workaround for intermittent async failure status from ai-library
    const showProgress = predictionLoading || (predictionAsync && predictionAsync.status !== "success");

    if (predictionError) {
      return this.renderError();
    } else if (showProgress) {
      return this.renderPending();
    } else if (prediction && prediction.records && prediction.records.length) {
      return this.renderResults();
    }
    return this.renderNoResults();
  };
}


function mapStateToProps(state) {
  return {
    ...state.flakeReducer,
    ...state.statusReducer
  };
}

export default connect(mapStateToProps)(FlakeWizardPrediction);
