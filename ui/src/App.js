import React from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Routes } from "./Routes"

import Notifications from "./Notifications/components/Notifications"
import { getStatus } from "./Status/actions";

import logo from "./static/img/opendatahub_logo.png"
import "./App.css";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showAboutModal: false
    }
  }

  componentDidMount() {
    this.props.init();
  }

  openAboutModal = (e) => {
    this.setState({showAboutModal: true});
  };

  closeAboutModal = (e) => {
    this.setState({showAboutModal: false});
  };

  render() {
    return (
      <div className="App">
        <nav className="navbar navbar-inverse navbar-fixed-top">
          <div className="container">
            <div className="navbar-header">
              <a className="navbar-brand" href="/">
                <img className="navbar-brand-name" src={logo} alt="AI Library"/>
              </a>
            </div>
          </div>
        </nav>
        <Routes/>
        <Notifications/>
      </div>
    );
  }
}


function mapStateToProps(state) {
  return state.statusReducer;
}

function mapDispatchToProps(dispatch) {
  return {
    init: () => {
      dispatch(getStatus());
    }
  };
}

App = connect(mapStateToProps, mapDispatchToProps)(App);

export default withRouter(App);
