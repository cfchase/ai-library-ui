import * as React from "react";

import { Icon } from "patternfly-react"

import { CopyToClipboard } from "react-copy-to-clipboard";
import ReactJson from "react-json-view";
import "./JsonSample.css"



class JsonSample extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showJson: !props.initHidden
    }
  };

  toggleDetails = () => {
    this.setState({showJson: !this.state.showJson})
  };

  shouldCollapse = field => {
    const limit = this.props.showArrayLimit || 3;
    return field.type === "array" && field.src.length > limit;
  };

  render() {
    const {title, object} = this.props;
    const { showJson } = this.state;

    const toggleDetailsText = showJson
      ? <span><Icon type="fa" name="chevron-down"/> Hide</span>
      : <span><Icon type="fa" name="chevron-right"/> Show</span>;
    const jsonSection = showJson ?
      <ReactJson
        name={false}
        enableClipboard={false}
        onEdit={false}
        onAdd={false}
        onDelete={false}
        displayDataTypes={false}
        collapseStringsAfterLength={120}
        shouldCollapse={field => this.shouldCollapse(field)}
        src={object}
      />
      : "";

    return (
      <div className="JsonSample json-sample-section">
        <div className="json-sample-heading">
          <h4 className="json-sample-title">{title}</h4>
          <CopyToClipboard
            text={JSON.stringify(object, undefined, 2)}>
            <a className="clickable-empty-link json-sample-action"><Icon type="fa" name="copy"/> Copy</a>
          </CopyToClipboard>
          <a className="clickable-empty-link json-sample-action" onClick={this.toggleDetails}>{toggleDetailsText}</a>
        </div>
        {jsonSection}
      </div>
    )
  }
}

export default JsonSample;

