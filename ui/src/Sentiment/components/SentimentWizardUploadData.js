import React from "react";
import { Field, reduxForm } from "redux-form";

import { sampleProductReview, sampleSpeech } from "../utilities"

export const validate = values => {
  const errors = {};

  if (!values.sentimentText) {
    errors.sentimentText = "Required"
  }

  return errors
};

class SentimentWizardUploadData extends React.Component {

  pasteSample = (e, sampleText) => {
    e.preventDefault();
    this.props.autofill("sentimentText", sampleText)
  };

  render() {
    return (
      <div>
        <h2>Try It</h2>
        <p>Try pasting in a conversation or article and hit next to see a sample analysis.
          Perhaps a <a className="clickable-empty-link" onClick={e => this.pasteSample(e, sampleProductReview)}>product review</a>
          , <a className="clickable-empty-link" onClick={e => this.pasteSample(e, sampleSpeech)}>famous speech</a>
          , or simply <a className="clickable-empty-link" onClick={e => this.pasteSample(e, "I love Red Hat.")}>"I love Red Hat"</a>.
        </p>
        <form className="sentiment-wizard-contents">
          <div className="form-group required">
            <label className="control-label">Text to analyze:</label>
            <Field className="form-control" id="sentimentTextInput" name="sentimentText" component="textarea"
                   rows="20"/>
          </div>
        </form>
      </div>
    );
  };
}

SentimentWizardUploadData = reduxForm({
  form: "SentimentWizardUploadData",
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  validate
})(SentimentWizardUploadData);

export default SentimentWizardUploadData
