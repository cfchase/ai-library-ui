addTrainedModelRoutes = (server) => {
  server.route({
    method: ["GET"],
    path: "/api/duplicate/models/{id}",
    handler: require("./get").handler
  });

  server.route({
    method: ["POST"],
    path: "/api/duplicate/models",
    handler: require("./post").handler,
    options: {
      validate: require("./post").validate
    }
  });
};

module.exports = addTrainedModelRoutes;