import * as React from "react";

import d3WordCloud from "../../Utilities/d3WordCloud"



class SentimentWordCloud extends React.Component {
  componentDidMount() {
    this._cloud = d3WordCloud.create(
      this._rootNode,
      this.props.data,
      this.props.config
    );
  }

  componentDidUpdate() {
    d3WordCloud.update(
      this._rootNode,
      this.props.data,
      this.props.config,
      this._cloud
    );
  }

  componentWillUnmount() {
    d3WordCloud.destroy(this._rootNode);
  }

  _setRef(componentNode) {
    this._rootNode = componentNode;
  }


  render() {
    return <div className="SentimentWordCloud" ref={this._setRef.bind(this)}/>;
  }
}

export default SentimentWordCloud;
