const urlJoin = require("url-join");
const Wreck = require("wreck");
const asyncStatusFromAiLibrary = require("../../utilities").asyncStatusFromAiLibrary;

const servicePrefix = require("../constants").servicePrefix;
const modelsPrefix = require("../constants").modelsPrefix;
const modelRecordsPrefix = require("../constants").modelRecordsPrefix;
const storageErrorResponseBody = require("../../utilities").storageErrorResponseBody;

handler = async (request, h) => {
  const storage = request.server.app.aiLibrary.storage;
  const id = request.params.id;
  const fileKey = `${servicePrefix}/${modelsPrefix}/${id}/duplicate-model.json`;
  const fileDir = `${servicePrefix}/${modelsPrefix}/${id}/${modelRecordsPrefix}`;
  const includeRecords = request.query.include === "records";

  let httpStatusCode = 200;
  let recordsExist = false;
  let data = null;

  try {

    data = await storage.readJson(fileKey);
    if (includeRecords) {
      let records = await storage.readJsonDir(fileDir);
      if (records) {
        recordsExist = records.length > 0;
        data.records = records;
      }
    } else {
      let recordList = await storage.ls(fileDir);
      recordsExist = recordList.Contents && recordList.Contents.length > 0;
    }
  }
  catch (err) {
    console.error(err);
    let responseBody = storageErrorResponseBody(err);
    return h.response(responseBody).code(responseBody.statusCode);
  }

  if (recordsExist) {
    return h.response({
      metadata: {
        type: "DuplicateModel",
        async: {
          status: "success"
        }
      },
      data
    }).code(httpStatusCode);
  }

  const pollRequestUrl = urlJoin(request.server.app.aiLibrary.url, "poll-status?blocking=true&result=true");
  const headers = {Authorization: request.server.app.aiLibrary.basicAuthHeader, ...request.headers};
  const name = request.params.id;

  let jobStatus = null;

  delete headers.host;
  delete headers["content-length"];

  try {
    const pollingResponse = await Wreck.request(
      "POST",
      pollRequestUrl,
      {
        payload: { name },
        headers: headers,
        rejectUnauthorized: false
      });

    const r = await Wreck.read(pollingResponse, {json: true});
    if (pollingResponse.statusCode > 299) {
      console.error(request);
      return h.response(r).code(pollingResponse.statusCode);
    }

    jobStatus = asyncStatusFromAiLibrary(r);

    //if the job is successful but the file is missing, we return 404;
    if (!recordsExist && jobStatus === "success") {
       return h.response().code(404);
    }

    return h.response({
      metadata: {
        type: "DuplicateModel",
        async: {
          status: jobStatus
        }
      },
      data
    }).code(httpStatusCode);
  }
  catch (err) {
    console.error(err);
    return h.response(err).code(err.statusCode);
  }
};

module.exports.handler = handler;
