const trainingData = {
  name: "sample",
  id: "sample",
  records: [
    {
      title: " RFE cinder Send notifications when attaching and detaching volumes",
      content: " Cloned from launchpad blueprint a href https blueprints launchpad net cinder spec attachment notifications https blueprints launchpad net cinder spec attachment notifications a \n\nDescription \n\nSend a notification when attaching and detaching volumes \n\nThose notifications would allow the volume status to be updated in Ceilometer\nand add the ability for an admin to search samples for volume status history \n\nWithout this notification if volume usage audit period configuration value for cinder volume usage audit is configured to hour a volume resource could still be considered quot available quot by Ceilometer although it has been attached 59 minutes and 59 seconds ago \n\n\nSpecification URL additional information \n\nNone\n\n\n Moving to POST based on upstream status Implemented \n\n\n Eric how can this be tested without Ceilometer \n\n\n In reply to Ayal Baron from a href show bug cgi id 1041487 c3 comment 3 a \n span class quote gt Eric how can this be tested without Ceilometer span \n\nDebug level logs should show AMQP messages going out for these events \n\nAlternatively any tool like qpid printevents that can attach to the AMQP bus and print messages should show these if they are enabled in Cinder \n\n\n verified on \npython cinderclient 1 0 8 1 el7ost noarch\nopenstack cinder 2014 1 4 el7ost noarch\npython cinder 2014 1 4 el7ost noarch\n\n\n Since the problem described in this bug report should be\nresolved in a recent advisory it has been closed with a\nresolution of ERRATA \n\nFor information on the advisory and where to find the updated\nfiles follow the link below \n\nIf the solution does not work for you open a new bug report \n\n a href http rhn redhat com errata RHEA 2014 0852 html http rhn redhat com errata RHEA 2014 0852 html a \n\n\n",
      id: "0a1c64a9-85c7-4cee-91bd-780d66ce70b5",
      createdAt: "2018-12-03T18:44:22.725Z",
      modifiedAt: "2018-12-03T18:44:22.725Z"
    },
    {
      title: "When Installing openstack cinder The package openstack utils shouldn t be installed by dependency ",
      content: " Description of problem \n \nWhen Installing openstack cinder The package openstack utils shouldn t be installed by dependency \n\n\nVersion Release number of selected component if applicable \n \nFolsom \n\n\nHow reproducible \n \nAlways \n\n\nSteps to Reproduce \n \n1 yum install openstack cinder\n \n\nActual results \n \n1 The package openstack utils installed \n\n\nExpected results \n \n1 The package openstack utils shouldn t be installed \n\n\n Verified With openstack cinder 2012 2 1 7 el6ost noarch\n\n yum install openstack cinder\nLoaded plugins product id rhnplugin security subscription manager\nThis system is not registered to Red Hat Subscription Management You can use subscription manager to register \nThis system is receiving updates from RHN Classic or RHN Satellite \nSetting up Install Process\nResolving Dependencies\n gt Running transaction check\n gt Package openstack cinder noarch 0 2012 2 1 7 el6ost will be installed\n gt Processing Dependency python cinder 2012 2 1 7 el6ost for package openstack cinder 2012 2 1 7 el6ost noarch\n gt Processing Dependency python keystone for package openstack cinder 2012 2 1 7 el6ost noarch\n gt Processing Dependency python cinderclient for package openstack cinder 2012 2 1 7 el6ost noarch\n gt Running transaction check\n gt Package python cinder noarch 0 2012 2 1 7 el6ost will be installed\n gt Processing Dependency python glanceclient gt 1 0 for package python cinder 2012 2 1 7 el6ost noarch\n gt Package python cinderclient noarch 0 0 2 26 1 el6 will be installed\n gt Package python keystone noarch 0 2012 2 1 3 el6ost will be installed\n gt Running transaction check\n gt Package python glanceclient noarch 1 0 5 1 1 el6 will be installed\n gt Processing Dependency python keystoneclient gt 1 0 1 2 for package 1 python glanceclient 0 5 1 1 el6 noarch\n gt Running transaction check\n gt Package python keystoneclient noarch 1 0 1 3 27 1 el6 will be installed\n gt Finished Dependency Resolution\n\nDependencies Resolved\n\n \n Package Arch Version Repository Size\n \nInstalling \n openstack cinder noarch 2012 2 1 7 el6ost packstack 0 35 k\nInstalling for dependencies \n python cinder noarch 2012 2 1 7 el6ost packstack 0 687 k\n python cinderclient noarch 0 2 26 1 el6 packstack 0 71 k\n python glanceclient noarch 1 0 5 1 1 el6 packstack 0 74 k\n python keystone noarch 2012 2 1 3 el6ost packstack 0 261 k\n python keystoneclient noarch 1 0 1 3 27 1 el6 packstack 0 67 k\n\nTransaction Summary\n \nInstall 6 Package s \n\nTotal download size 1 2 M\nInstalled size 7 6 M\nIs this ok y N y\nDownloading Packages \n 1 6 openstack cinder 2012 2 1 7 el6ost noarch rpm 35 kB 00 00 \n 2 6 python cinder 2012 2 1 7 el6ost noarch rpm 687 kB 00 00 \n 3 6 python cinderclient 0 2 26 1 el6 noarch rpm 71 kB 00 00 \n 4 6 python glanceclient 0 5 1 1 el6 noarch rpm 74 kB 00 00 \n 5 6 python keystone 2012 2 1 3 el6ost noarch rpm 261 kB 00 00 \n 6 6 python keystoneclient 0 1 3 27 1 el6 noarch rpm 67 kB 00 00 \n \nTotal 15 MB s 1 2 MB 00 00 \nRunning rpm check debug\nRunning Transaction Test\nTransaction Test Succeeded\nRunning Transaction\n Installing python keystone 2012 2 1 3 el6ost noarch 1 6 \n Installing python cinderclient 0 2 26 1 el6 noarch 2 6 \n Installing 1 python keystoneclient 0 1 3 27 1 el6 noarch 3 6 \n Installing 1 python glanceclient 0 5 1 1 el6 noarch 4 6 \n Installing python cinder 2012 2 1 7 el6ost noarch 5 6 \n Installing openstack cinder 2012 2 1 7 el6ost noarch 6 6 \n Verifying 1 python keystoneclient 0 1 3 27 1 el6 noarch 1 6 \n Verifying python cinder 2012 2 1 7 el6ost noarch 2 6 \n Verifying python cinderclient 0 2 26 1 el6 noarch 3 6 \n Verifying openstack cinder 2012 2 1 7 el6ost noarch 4 6 \n Verifying 1 python glanceclient 0 5 1 1 el6 noarch 5 6 \n Verifying python keystone 2012 2 1 3 el6ost noarch 6 6 \n\nInstalled \n openstack cinder noarch 0 2012 2 1 7 el6ost \n\nDependency Installed \n python cinder noarch 0 2012 2 1 7 el6ost python cinderclient noarch 0 0 2 26 1 el6 python glanceclient noarch 1 0 5 1 1 el6 python keystone noarch 0 2012 2 1 3 el6ost \n python keystoneclient noarch 1 0 1 3 27 1 el6 \n\nComplete \n\n\n I was wondering about the exact rationale for this change \nnova glance quantum all require openstack utils as a convenience\nso that one can do \n\n yum install openstack cinder\n openstack db init service cinder\n\nI presume the same rationale would apply to the other services \nI notce that keystone doesn t depend on openstack utils \nso we should probably make all services equal in this regard \n\n\n Since the problem described in this bug report should be\nresolved in a recent advisory it has been closed with a\nresolution of ERRATA \n\nFor information on the advisory and where to find the updated\nfiles follow the link below \n\nIf the solution does not work for you open a new bug report \n\n a href http rhn redhat com errata RHBA 2013 0260 html http rhn redhat com errata RHBA 2013 0260 html a \n\n\n In reply to P draig Brady from a href show bug cgi id 881610 c4 comment 4 a \n span class quote gt I was wondering about the exact rationale for this change \n gt nova glance quantum all require openstack utils as a convenience\n gt so that one can do \n gt \n gt yum install openstack cinder\n gt openstack db init service cinder\n gt \n gt I presume the same rationale would apply to the other services \n gt I notce that keystone doesn t depend on openstack utils \n gt so we should probably make all services equal in this regard span \n\nThis is not a hard dependency but I do agree that if all other services install openstacl utils by dependency for convenience openstack cinder should not be any different \n\n\n",
      id: "0a757ec3-3dec-4cf4-a7de-053b27342c2b",
      createdAt: "2018-12-03T18:44:23.757Z",
      modifiedAt: "2018-12-03T18:44:23.757Z"
    },
    {
      title: "Cinder 3PAR driver fails ungracefully if session count exceeded requiring operator intervention to resolve inconsistency Failure should be graceful and not require manual clean up",
      content: " Description of problem \n\nThe HP3PAR Cinder driver opens a REST connection to a web service listening on a 3PAR SAN to request LUNs When running out of sessions during load testing which is normal as there will be limits the action desired on the cinder volume should be denied Instead it seems cinder attempts to make a change e g create a volume but fails midway leaving an inconsistent state \n\n Version Release number of selected component if applicable \nopenstack cinder 2014 1 5 1 1 el6ost a class bz bug link \n bz status CLOSED bz closed \n title CLOSED ERRATA Controller node does not fully detach multipath device and the device can not be removed by manual means \n href show bug cgi id 1255523 bz1255523 a as supported hotfix\n\n How reproducible \nCustomer can reproduce via a script in his environment \n\n Steps to Reproduce \nSpawn 4 concurrent threads of the following to create delete 64 VMs Keep track of the number of sessions along the way \n\nfor x in range 0 16 \n vol cinder create \n vm nova boot vol \n nova delete vm \n cinder delete vol \n print session overview \n\n Actual results \n cinder list shows volumes in error states or available states of volume type None which need to be manually deleted by the operator \n\n Expected results \nIf no more sessions are available on the SAN then the cinder request should fail \n\nAdditional info \n\nIn BZs 1270125 and 1255523 we didn t hit this problem under the same load test when using a single controller This happens with HA I ve verified that openstack cinder volume is not running active active as per a class bz bug link \n bz status CLOSED bz closed \n title CLOSED ERRATA Cinder Volume HA active active should be avoided \n href show bug cgi id 1132722 BZ 1132722 a \n\n\n Workaround modifying the test to boot instances with \n\n nova boot block device source volume id 7f shutdown preserve \n\nlet the customer achieve a 98 success rate on an 8x32 test 8 threads 32 times and only 3 failures I will share what we ve discovered with HP for feedback \n\n\n",
      id: "0a840fc7-fbd2-4b35-94ae-14f1208a1f28",
      createdAt: "2018-12-03T18:44:24.188Z",
      modifiedAt: "2018-12-03T18:44:24.188Z"
    },
    {
      title: " RFE cinder Enable ceate a volume on force host",
      content: " Cloned from launchpad blueprint a href https blueprints launchpad net cinder spec cinder force host https blueprints launchpad net cinder spec cinder force host a \n\nDescription \n\nCinder scheduler will help select host for create request based on scheduler filters but end user cannot create volume directly on a force host \n\nIt is better that we add this feature to enable end user can create volumes on a force host \n\nSpecification URL additional information \n\nNone\n\n\n Patch was abandoned upstream\n\n\n",
      id: "0a1076cf-47e9-4924-9813-90c48dd2ba95",
      createdAt: "2018-12-03T18:44:22.728Z",
      modifiedAt: "2018-12-03T18:44:22.728Z"
    },
    {
      title: "Migrated instances from VMware can not boot because root filesystem is set bootable false",
      content: " Description of problem \n\nMigrated instances from VMware using virt v2v can not boot because root filesystem is set bootable false\n\nVersion Release number of selected component if applicable \n\nOSP 5 0 on RHEL 6\n\nHow reproducible \n\n100 \n\nSteps to Reproduce \n1 \n2 \n3 \n\nActual results \n\n\nExpected results \n\nWe should backport a href https review openstack org c 84057 https review openstack org c 84057 a so that the volumes created is set bootable true automatically\n\nAdditional info \n\n\n Since the problem described in this bug report should be\nresolved in a recent advisory it has been closed with a\nresolution of ERRATA \n\nFor information on the advisory and where to find the updated\nfiles follow the link below \n\nIf the solution does not work for you open a new bug report \n\n a href https access redhat com errata RHBA 2016 1193 https access redhat com errata RHBA 2016 1193 a \n\n\n",
      id: "0b1ac7fa-9625-4d7c-8498-bcff03426e78",
      createdAt: "2018-12-03T18:44:24.158Z",
      modifiedAt: "2018-12-03T18:44:24.158Z"
    },
    {
      title: "Cinder log level keeps changing every few seconds",
      content: " Created span class a href attachment cgi id 1395411 name attach 1395411 title Cinder logs attachment 1395411 a a href attachment cgi id 1395411 amp action edit title Cinder logs details a span \nCinder logs\n\nDescription of problem \nAfter Setting log level for cinder services \nthe log level keeps changing every few seconds \n\nVersion Release number of selected component if applicable \nopenstack cinder 12 0 0 0 20180122233816 71b869c el7ost noarch\npuppet cinder 12 2 0 0 20180123011607 277828c el7ost noarch\npython2 cinderclient 3 4 0 0 20180110044822 36a2f4b el7ost noarch\npython cinder 12 0 0 0 20180122233816 71b869c el7ost noarch\n\nHow reproducible \n100 \n\nSteps to Reproduce \n\n1 Get current log levels for services\n\n overcloud stack 64 undercloud 0 cinder os volume api version 3 32 service get log\n \n Binary Host Prefix Level \n \n cinder api hostgroup castellan common utils DEBUG \n cinder api hostgroup castellan key manager barbican key manager DEBUG \n cinder api hostgroup cinder api DEBUG \n\n2 Set log level quot WARNING quot for cinder services\n\n overcloud stack 64 undercloud 0 cinder os volume api version 3 32 service set log WARNING\n\n3 Get log levels for services several times\n\n overcloud stack 64 undercloud 0 cinder os volume api version 3 32 service get log\n \n Binary Host Prefix Level \n \n cinder api hostgroup castellan common utils WARNING \n cinder api hostgroup castellan key manager barbican key manager WARNING \n cinder api hostgroup cinder api WARNING \n\n overcloud stack 64 undercloud 0 cinder os volume api version 3 32 service get log\n \n Binary Host Prefix Level \n \n cinder api hostgroup castellan common utils DEBUG \n cinder api hostgroup castellan key manager barbican key manager DEBUG \n cinder api hostgroup cinder api DEBUG \n\nActual results \nCinder log level keeps changing every few seconds\n\nExpected results \nCinder log level should be quot WARNING quot without changing\n\n\n API services have a limitation when running behind a load balancer and what you are seeing are the log levels of the different API services you have running behind the load balancer \n\nI explained this limitation in the specs 1 \n\n Setting the log levels will be possible for all Volume Scheduler and Backup services but limited in the API service to only the service process that receives the request since there is no mechanism in place right now to propagate the request to other API nodes \n\n 1 a href https specs openstack org openstack cinder specs specs pike dynamic log levels html https specs openstack org openstack cinder specs specs pike dynamic log levels html a \n",
      id: "0bd07144-66a5-40bf-93eb-be6a1990ec8d",
      createdAt: "2018-12-03T18:44:23.809Z",
      modifiedAt: "2018-12-03T18:44:23.809Z"
    },
    {
      title: "Fix Lun ID 0 in HPE 3PAR driver",
      content: " Description of problem \n\nPlease backport and pull downstream the following HPE 3PAR driver patch \n\n a href https git openstack org cgit openstack cinder commit id 4649d3d830c3210734a75c8c5c0a733ddb5e694c https git openstack org cgit openstack cinder commit id 4649d3d830c3210734a75c8c5c0a733ddb5e694c a \n\nWe got a confirmation that this fixes indeed the issue in RHOS 9 \n\n\n Since the problem described in this bug report should be\nresolved in a recent advisory it has been closed with a\nresolution of ERRATA \n\nFor information on the advisory and where to find the updated\nfiles follow the link below \n\nIf the solution does not work for you open a new bug report \n\n a href https access redhat com errata RHBA 2018 2136 https access redhat com errata RHBA 2018 2136 a \n\n\n",
      id: "0c42d491-0254-4ec9-abd4-d5ef2d9a9afb",
      createdAt: "2018-12-03T18:44:23.782Z",
      modifiedAt: "2018-12-03T18:44:23.782Z"
    },
    {
      title: "GlusterFS Cinder driver should store format of volume",
      content: " The Cinder GlusterFS driver supports qcow2 and raw formatted volumes but uses autodetection which may be unsafe in the case of a qcow2 header written to a raw volume The format should be stored at creation time and updated as needed such as when a snapshot merge occurs \n\n\n Upstream ML discussion \n\n a href http lists openstack org pipermail openstack dev 2014 June 038407 html http lists openstack org pipermail openstack dev 2014 June 038407 html a \n\n\n",
      id: "0c718186-703b-4bd7-abf8-90bf2bef85c5",
      createdAt: "2018-12-03T18:44:24.563Z",
      modifiedAt: "2018-12-03T18:44:24.563Z"
    },
    {
      title: "Dell SC Add exclude domain ip option",
      content: " Description of problem \nBackport this to mitaka\n a href https review openstack org c 359328 https review openstack org c 359328 a \nIf a domain iscsi portal should NOT be returned this allows\nthe user to specify that Otherwise all domains are returned \n\nVersion Release number of selected component if applicable \n\n\nHow reproducible \n\n\nSteps to Reproduce \n1 Add multiple fault domains and exclude using exclude domain ip option in cinder conf\n2 \n3 \n\nActual results \n\n\nExpected results \n\n\nAdditional info \n\n\n Can Dell provide a patch that applies to Mitaka for this change It s tied in with other Newton changes and is not a very straightforward backport for something that I can t test directly \n\n\n Tom Swanson irc swanson is working on it Will update the bug when he pushes the cherry pick\n\n\n a href https review openstack org c 366321 https review openstack org c 366321 a posted\n\n\n The cherry pick right one\n a href https review openstack org c 366841 https review openstack org c 366841 a \n\n\n This is merged in Mitaka and ready to go\n\n\n This will be pulled in via the Cinder 8 1 1 rebase for OSP9 \n\n\n Since the problem described in this bug report should be\nresolved in a recent advisory it has been closed with a\nresolution of ERRATA \n\nFor information on the advisory and where to find the updated\nfiles follow the link below \n\nIf the solution does not work for you open a new bug report \n\n a href https rhn redhat com errata RHBA 2016 2030 html https rhn redhat com errata RHBA 2016 2030 html a \n\n\n When can we expect to get this package in OSP 9 The latest bits still have \nopenstack cinder 8 1 0 1 el7ost not openstack cinder 8 1 1 1 el7ost\n\n\n This has been shipped in OSP10\n\n\n This has been shipped in OSP10 Can be closed\n\n\n",
      id: "0cebb0f5-d3df-4dfe-a21a-2334821e2cbd",
      createdAt: "2018-12-03T18:44:22.856Z",
      modifiedAt: "2018-12-03T18:44:22.856Z"
    },
    {
      title: " RFE cinder backup driver for Google cloud storage",
      content: " Add a Cinder backup driver for Google cloud storage to extend OpenStack data protection to the public cloud \n\nUse cases include \n creating backup which is stored on Google Cloud Storage\n restoring backup from Google cloud storage\n incremental backup\n independent of any volume backend\n\nBlueprint a href https blueprints launchpad net cinder spec gcs cinder backup driver https blueprints launchpad net cinder spec gcs cinder backup driver a \n\n\n Additional dependencies on oauth2client and httplib2 need to be added to our packaging for this \n\n\n This bug was accidentally moved from POST to MODIFIED via an error in automation please see a href mailto mmccune 64 redhat com mmccune 64 redhat com a with any questions\n\n\n Since the problem described in this bug report should be\nresolved in a recent advisory it has been closed with a\nresolution of ERRATA \n\nFor information on the advisory and where to find the updated\nfiles follow the link below \n\nIf the solution does not work for you open a new bug report \n\n a href https rhn redhat com errata RHEA 2016 1761 html https rhn redhat com errata RHEA 2016 1761 html a \n\n\n",
      id: "0d23558b-52f8-422f-be7e-6b7bc50b47f7",
      createdAt: "2018-12-03T18:44:24.173Z",
      modifiedAt: "2018-12-03T18:44:24.173Z"
    }
  ]
};

const model = {
  id: "sample",
  trainingDataId: "sample",
  createdAt: "2018-12-03T19:54:18.189Z",
  modifiedAt: "2018-12-03T19:54:18.189Z"
};

const predictionData =  {
    name: "sample",
    id: "sample",
    createdAt: "2018-12-03T21:19:32.495Z",
    modifiedAt: "2018-12-03T21:19:32.495Z",
    records: [
      {
        title: "thinlvm reports free space of outer VG rather than inner POOL, causing new volumes creation to fail",
        content: "Description of problem: thinlvm driver returns no free space when the VG containing the POOL is full but that is a side effect of the POOL consuming up all the VG space; it should probably report about the POOL free space Version-Release number of selected component (if applicable): openstack-cinder-2013.2-3.el6ost.noarch Steps to Reproduce: 1. configure cinder with lvm_type=thin 2. make sure the cinder-volumes-pool is extended to 100% of the outer VG 3. new volumes creation will fail as the volume service reports no free space",
        id: "96b50810-1645-4e6a-97f8-17a4275ccfa6",
        createdAt: "2018-12-03T21:19:32.551Z",
        modifiedAt: "2018-12-03T21:19:32.551Z"
      },
      {
        title: " Add init scripts for cinder-backup",
        content: "The cinder-backup service needs to have init scripts added to support volume backup functionality",
        id: "b0bee6ad-7252-45a5-8230-168249e2553f",
        createdAt: "2018-12-03T21:19:32.547Z",
        modifiedAt: "2018-12-03T21:19:32.547Z"
      },
      {
        title: "Externd glusterfs cinder plugin to use libgapi and QEMU block driver integration",
        content: "Description of problem: BZ 840987 enables storage of live VM images and virtual block store on Red Hat Storage (GlusterFS) volumes with libgfapi and QEMU block driver. This offers greater flexibility and better performance in certain kinds of workloads (for example, IO-intensive workloads). This BZ requests that the glusterfs cinder plugin be extended to use libgfapi and QEMU block driver.",
        id: "b1524002-f4a5-48e8-8f36-a18f9e913635",
        createdAt: "2018-12-03T21:19:32.555Z",
        modifiedAt: "2018-12-03T21:19:32.555Z"
      }
    ],
  };

const prediction = {
  id: "sample",
  modelId: "sample",
  predictionDataId: "sample",
  createdAt: "2018-12-03T21:19:32.495Z",
  modifiedAt: "2018-12-03T21:19:32.495Z",
  records: [
    {
      title: "thinlvm reports free space of outer VG rather than inner POOL, causing new volumes creation to fail",
      content: "Description of problem: thinlvm driver returns no free space when the VG containing the POOL is full but that is a side effect of the POOL consuming up all the VG space; it should probably report about the POOL free space Version-Release number of selected component (if applicable): openstack-cinder-2013.2-3.el6ost.noarch Steps to Reproduce: 1. configure cinder with lvm_type=thin 2. make sure the cinder-volumes-pool is extended to 100% of the outer VG 3. new volumes creation will fail as the volume service reports no free space",
      id: "2b700aa4-7c53-4501-a315-109ddc7b7ef8",
      createdAt: "2019-02-04T18:04:03.607Z",
      modifiedAt: "2019-02-04T18:04:03.607Z",
      duplicateBugs: [
        {
          id: "6e97863e-2517-4949-9da5-5cb20fbbf432.json",
          title: "cinder thinlvm allocates new snap based volumes outside the pool"
        },
        {
          id: "8b13c98b-4be8-4564-8cdf-007a5a38564a.json",
          title: "thinlvm driver fails to create the pool if the outer vg is rounded to the GB"
        },
        {
          id: "9af99e23-a4df-4eea-8496-5fb5b1d63377.json",
          title: "ThinLVM space calculation fails if thin pool LV is not activated"
        },
        {
          id: "e8c58a9f-8adb-4158-ba1c-9361a4f79e7e.json",
          title: "ThinLVM attempts to create a cinder volumes pool big as much as cinder volumes and fails"
        },
        {
          id: "5da73d58-7301-46bd-a0c7-9778fe38777a.json",
          title: "Creating thin LVM volume fails due to incorrect thin pool free space calculation"
        },
        {
          id: "8b13c98b-4be8-4564-8cdf-007a5a38564a.json",
          title: "thinlvm driver fails to create the pool if the outer vg is rounded to the GB"
        },
        {
          id: "8b13c98b-4be8-4564-8cdf-007a5a38564a.json",
          title: "thinlvm driver fails to create the pool if the outer vg is rounded to the GB"
        },
        {
          id: "18c4b2a6-7244-4d73-9800-f8b229102c30.json",
          title: "cinder volume creation fails and does not return an error "
        },
        {
          id: "18c4b2a6-7244-4d73-9800-f8b229102c30.json",
          title: "cinder volume creation fails and does not return an error "
        },
        {
          id: "7570ef3c-4a77-4ea2-ac13-5dd47d6942c7.json",
          title: "ThinLVM pool created with incorrect size"
        }
      ]
    }
  ]
};

export {
  trainingData,
  model,
  predictionData,
  prediction
};
