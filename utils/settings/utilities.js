const generateAuthHeader = (username, password) => {
  let authStr = Buffer.from(`${username}:${password}`).toString("base64");
  return `Basic ${authStr}`;
};


const getEnv = (varName) => {
  const value = process.env[varName];

  if (!value) {
    throw `${varName} environment variable missing.`;
  }

  return value;
};

module.exports.generateAuthHeader = generateAuthHeader;
module.exports.getEnv = getEnv;