const urlJoin = require("url-join");
const Wreck = require("wreck");
const asyncStatusFromAiLibrary = require("../../utilities").asyncStatusFromAiLibrary;

const servicePrefix = require("../constants").servicePrefix;
const modelsPrefix = require("../constants").modelsPrefix;
const storageErrorResponseBody = require("../../utilities").storageErrorResponseBody;

handler = async (request, h) => {
  const storage = request.server.app.aiLibrary.storage;
  const id = request.params.id;
  const modelLocation = `${servicePrefix}/${modelsPrefix}/${id}/flake.model`;
  const jsonLocation = `${servicePrefix}/${modelsPrefix}/${id}/flake-model.json`;

  let httpStatusCode = 200;
  let fileExists = false;
  let data = null;

  try {
    fileExists = await storage.fileExists(modelLocation);
    data = await storage.readJson(jsonLocation);
  }
  catch (err) {
    console.error(err);
    let responseBody = storageErrorResponseBody(err);
    return h.response(responseBody).code(responseBody.statusCode);
  }

  if (fileExists) {
    return h.response({
      metadata: {
        type: "FlakeModel",
        async: {
          status: "success"
        }
      },
      data
    }).code(httpStatusCode);
  }

  const pollRequestUrl = urlJoin(request.server.app.aiLibrary.url, "poll-status?blocking=true&result=true");
  const headers = {Authorization: request.server.app.aiLibrary.basicAuthHeader, ...request.headers};
  const name = request.params.id;

  let jobStatus = null;

  delete headers.host;
  delete headers["content-length"];

  try {
    const pollingResponse = await Wreck.request(
      "POST",
      pollRequestUrl,
      {
        payload: { name },
        headers: headers,
        rejectUnauthorized: false
      });

    const r = await Wreck.read(pollingResponse, {json: true});
    if (pollingResponse.statusCode > 299) {
      console.error(request);
      return h.response(r).code(pollingResponse.statusCode);
    }

    jobStatus = asyncStatusFromAiLibrary(r);

    //if the job is successful but the file is missing, we return 404;
    if (!fileExists && jobStatus === "success") {
       return h.response().code(404);
    }

    return h.response({
      metadata: {
        type: "FlakeModel",
        async: {
          status: jobStatus
        }
      },
      data
    }).code(httpStatusCode);
  }
  catch (err) {
    console.error(err);
    return h.response(err).code(err.statusCode);
  }
};

module.exports.handler = handler;
