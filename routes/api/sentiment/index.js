addSentimentRoutes = (server) => {
  server.route({
    method: ["GET"],
    path: "/api/sentiment/{id}",
    handler: require("./get").handler
  });

  server.route({
    method: ["POST"],
    path: "/api/sentiment",
    handler: require("./post").handler
  });
};

module.exports = addSentimentRoutes;