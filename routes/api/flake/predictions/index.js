addPredictionRoutes = (server) => {
  require("./records")(server);

  server.route({
    method: ["GET"],
    path: "/api/flake/predictions/{id}",
    handler: require("./get").handler
  });

  server.route({
    method: ["POST"],
    path: "/api/flake/predictions",
    handler: require("./post").handler
  });
};

module.exports = addPredictionRoutes;