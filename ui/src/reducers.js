import { combineReducers } from "redux";
import { statusReducer } from "./Status/reducers";
import { homeReducer } from "./Home/reducers";
import { notificationsReducer } from "./Notifications/reducers";
import { sentimentReducer } from "./Sentiment/reducers";
import { flakeReducer } from "./Flake/reducers";
import { duplicateBugReducer } from "./DuplicateBug/reducers";
import { reducer as formReducer } from "redux-form";

const rootReducer = combineReducers({
  statusReducer: statusReducer,
  homeReducer: homeReducer,
  notificationsReducer: notificationsReducer,
  sentimentReducer: sentimentReducer,
  flakeReducer: flakeReducer,
  duplicateBugReducer: duplicateBugReducer,
  form: formReducer
});

export default rootReducer;
