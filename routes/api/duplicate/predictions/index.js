addPredictionRoutes = (server) => {
  require("./records")(server);

  server.route({
    method: ["GET"],
    path: "/api/duplicate/predictions/{id}",
    handler: require("./get").handler
  });

  server.route({
    method: ["POST"],
    path: "/api/duplicate/predictions",
    handler: require("./post").handler
  });
};

module.exports = addPredictionRoutes;