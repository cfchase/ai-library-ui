import * as React from "react";
import lodashGet from "lodash/get";

import {
  Grid,
  Tab,
  Tabs
} from "patternfly-react";
import { connect } from "react-redux";

import { calculateSentiment, generateGetCurl, generatePostCurl } from "../utilities";

import SentimentWordCloud from "./SentimentWordCloud";
import SentimentLegend from "./SentimentLegend";
import SentimentDataTable from "./SentimentDataTable";
import AxiosError from "../../AxiosError/components/AxiosError";
import CodeSample from "../../CodeSample/components/CodeSample";
import JsonSample from "../../JsonSample/components/JsonSample";

import { getSentimentIgnoreNotFound } from "../actions";

const pollingInterval = 5000;

class SentimentWizardResults extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tabActiveKey: 0
    }
  }

  componentDidMount() {
    this.setState({
      pollingTimer: setInterval(() => {
          if (this.props.sentiment && this.props.sentiment.id
            && !this.props.sentimentLoading
            && this.props.sentimentAsync && this.props.sentimentAsync.status === "in_progress") {
            this.props.getSentiment(this.props.sentiment.id);
          }
        },
        pollingInterval)
    });
  }

  componentWillUnmount() {
    clearInterval(this.state.pollingTimer);
  }

  selectTab = (tabActiveKey) => {
    this.setState({tabActiveKey});
  };

  renderError = () => {
    return <AxiosError error={this.props.sentimentError}/>
  };

  renderPending = () => {
    return (
      <div className="sentiment-wizard-contents sentiment-results sentiment-pending">
        <div>
          <span className="spinner spinner-xl spinner-inline"/>
        </div>
      </div>
    )
  };

  renderResults = () => {
    const maxSize = 60;
    const {sentiment, createSentimentResponse, getSentimentResponse, wizardForm, status} = this.props;
    const entities = sentiment.entities;
    const sentimentText = lodashGet(wizardForm, "values.sentimentText");
    const baseUrl = lodashGet(status, "baseUrl");

    const {tabActiveKey} = this.state;

    const maxCount = Math.max(...entities.map(s => s.count));
    const baseSize = Math.round(maxSize / (Math.sqrt(maxCount)));

    let wordCloudConfig = {
      textFn: entity => entity.name,
      sizeFn: entity => (Math.sqrt(entity.count)) * baseSize,
      colorFn: entity => {
        switch (calculateSentiment(entity)) {
          case "verypositive":
            return "#3f9c35";
          case "positive":
            return "#0088ce";
          case "neutral":
            return "#8b8d8f";
          case "negative":
            return "#ec7a08";
          default:
            return "#cc0000"
        }
      }
    };

    const postCurl = generatePostCurl(baseUrl, sentimentText);
    const getCurl = generateGetCurl(baseUrl, sentiment.id);

    return (
      <div className="sentiment-wizard-contents sentiment-results sentiment-success">
        <Tabs
          activeKey={tabActiveKey}
          onSelect={this.selectTab}
          animation={false}
          id="sentimentResultsTabs"
        >
          <Tab eventKey={0} title="Visual">
            <SentimentWordCloud
              data={entities}
              config={wordCloudConfig}
            />
            <SentimentLegend/>
            <SentimentDataTable data={entities}/>
          </Tab>
          <Tab eventKey={1} title="Data View">
            <div className="sentiment-info-section">
              <Grid.Row>
                <Grid.Col className="sample-request-col" lg={6}>
                  <CodeSample title="POST Request" code={postCurl} language="bash"/>
                </Grid.Col>
                <Grid.Col className="sample-response-col" lg={6}>
                  <JsonSample title="POST Response" object={createSentimentResponse}/>
                </Grid.Col>
              </Grid.Row>
            </div>
            <div className="sentiment-info-section">
              <Grid.Row>
                <Grid.Col className="sample-request-col" lg={6}>
                  <CodeSample title="GET Request" code={getCurl} language="bash"/>
                </Grid.Col>
                <Grid.Col className="sample-response-col" lg={6}>
                  <JsonSample title="GET Response" object={getSentimentResponse}/>
                </Grid.Col>
              </Grid.Row>
            </div>
          </Tab>
        </Tabs>

      </div>
    );
  };

  renderNoResults = () => {
    return (
      <div className="sentiment-wizard-contents sentiment-results sentiment-no-results">
        <h3>No Results</h3>
      </div>
    );
  };

  render() {
    const {sentiment, sentimentAsync, sentimentLoading, sentimentError} = this.props;
    const showProgress = sentimentLoading || (sentimentAsync && sentimentAsync.status === "in_progress");

    if (sentimentError) {
      return this.renderError();
    } else if (showProgress) {
      return this.renderPending();
    } else if (sentiment && sentiment.entities && sentiment.entities.length) {
      return this.renderResults();
    }
    return this.renderNoResults();
  };
}

function mapStateToProps(state) {
  return {
    ...{wizardForm: state.form.SentimentWizardUploadData},
    ...state.sentimentReducer,
    ...state.statusReducer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getSentiment: (sentimentId) => {
      dispatch(getSentimentIgnoreNotFound(sentimentId));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SentimentWizardResults);


