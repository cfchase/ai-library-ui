addDataRecordRoutes = (server) => {
  server.route({
    method: ["GET"],
    path: "/api/flake/prediction-data/{setId}/records/{id}",
    handler: require("./get").handler
  });

  server.route({
    method: ["POST"],
    path: "/api/flake/prediction-data/{setId}/records",
    handler: require("./post").handler,
    options: {
      validate: require("./post").validate
    }
  });
};

module.exports = addDataRecordRoutes;