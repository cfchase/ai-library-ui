import React from "react";
import { connect } from "react-redux";

import { Grid, Tab, Tabs } from "patternfly-react";

import JsonSample from "../../JsonSample/components/JsonSample";
import CodeSample from "../../CodeSample/components/CodeSample";
import { generateCurl, predictionData } from "./sampleData";
import PredictionDataTableGrid from "./PredictionDataTableGrid";


class FlakeWizardPredictionData extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      tabActiveKey: 0
    }
  }

  selectTab = (tabActiveKey) => {
    this.setState({tabActiveKey});
  };

  renderPending = () => {
    return (
      <div className="flake-wizard-contents flake-results flake-pending">
        <div>
          <span className="spinner spinner-xl spinner-inline"/>
        </div>
      </div>
    )
  };

  render() {
    const {tabActiveKey} = this.state;
    const {status, createPredictionDataResponse, createPredictionRecordResponse} = this.props;

    if (!createPredictionDataResponse || !createPredictionRecordResponse) {
      return this.renderPending();
    }

    const createPredictionDataCurl = generateCurl(
      'POST',
      status.baseUrl,
      "api/flake/prediction-data");

    const createPredictionRecordCurl = generateCurl(
      'POST',
      status.baseUrl,
      `api/flake/prediction-data/${createPredictionDataResponse.data.id}/records`,
      predictionData[0]);

    return (
      <div className="flake-wizard-contents flake-sample">
        <Tabs
          activeKey={tabActiveKey}
          onSelect={this.selectTab}
          animation={false}
          id="flakeResultsTabs"
        >
          <Tab eventKey={0} title="Data">
            <div className="flake-info-section">
              <h2>Prediction Data</h2>
              <p>Prediction data includes the logs to be analyzed and a label to help users identify the test. Since we
                don't yet know if the test is a flake, that field would not be present.</p>
              <JsonSample title="Prediction Data Record (1 of 10)" object={predictionData[0]}/>
              <h3>Demo Prediction Data</h3>
              <p>As part of this demonstration, we created 10 new data records to analyze.</p>
              <PredictionDataTableGrid />
            </div>
          </Tab>
          <Tab eventKey={1} title="API">
            <div className="flake-info-section">
              <h2>Using the API - Creating Prediction Data Sets and Records</h2>
              <p>To create a prediction, we first to create the data set to process. Like training data, first create
                the prediction data set and then add records. We'll then use the prediction data set when running our
                prediction.</p>
              <h3>Create a Prediction Data Set</h3>
              <p>As part of this demonstration, we created a new data set.</p>
                <Grid.Row>
                <Grid.Col className="sample-request-col" lg={6}>
                  <CodeSample title="Request" code={createPredictionDataCurl} language="bash"/>
                </Grid.Col>
                <Grid.Col className="sample-response-col" lg={6}>
                  <JsonSample title="Response" object={createPredictionDataResponse}/>
                </Grid.Col>
              </Grid.Row>
              <h3>Create a Prediction Data Record</h3>
              <p>We then added 10 prediction data records as part of that data set.</p>
              <Grid.Row>
                <Grid.Col className="sample-request-col" lg={6}>
                  <CodeSample title="Request (1 of 10)" code={createPredictionRecordCurl} language="bash"/>
                </Grid.Col>
                <Grid.Col className="sample-response-col" lg={6}>
                  <JsonSample title="Response (1 of 10)" object={createPredictionRecordResponse}/>
                </Grid.Col>
              </Grid.Row>
            </div>
          </Tab>
        </Tabs>
      </div>
    );
  };
}

function mapStateToProps(state) {
  return {
    ...state.flakeReducer,
    ...state.statusReducer
  };
}

export default connect(mapStateToProps)(FlakeWizardPredictionData);
